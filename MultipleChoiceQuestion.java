import javax.swing.JOptionPane;

/**
*
* MultipleChoiceQuestion class
*
* it is used to represent a multiple choice question
*
*/
class MultipleChoiceQuestion extends Question{
    public MultipleChoiceQuestion(String query,String a,String b,String c,String d,String e, String correct){
        this.query=query; // question to ask
        this.correct=correct; // correct reply
        this.query += "\n" + a + ",\n";
        this.query += b + ",\n";
        this.query += c + ",\n";
        this.query += d + ",\n";
        this.query += e;
        this.nCorrect=0;
        this.valid="ABCDE";
    }
    public String ask(){ // ask question and check valid reply
        String reply="";
        for(;1==1;){ // keep looping until reply valid
            reply = JOptionPane.showInputDialog(this.query);
            reply = reply.toUpperCase();
            if(reply.length() == 1 && this.valid.contains(reply)){ // reply valid, break loop
                break;
            } else { // reply not valid, keep loop
                JOptionPane.showMessageDialog(null, "Please try again. Reply is not valid!");
            }
        }
        return reply;
    }
    public void check(){ // check if reply correct
        String reply="";
        reply=this.ask();
        if(reply.equals(this.correct)){
            JOptionPane.showMessageDialog(null, "Correct!");
            this.nCorrect++;
        }else{
            JOptionPane.showMessageDialog(null, "Incorrect. The correct reply is "+this.correct+"!");
        }
    }
}
