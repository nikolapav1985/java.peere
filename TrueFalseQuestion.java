import javax.swing.JOptionPane;

/**
*
* TrueFalseQuestion class
*
* it is used to represent a true false choice question
*
*/
class TrueFalseQuestion extends Question{
    public TrueFalseQuestion(String query, String correct){
        this.query="TRUE or FALSE: ".concat(query); // question to ask
        this.correct=correct; // correct reply
        this.valid="TRUE,FALSE,YES,NO,T,F,Y,N"; // options that are valid
    }
    public String ask(){ // ask question and check valid reply
        String reply="";
        String[] validArr=this.valid.split(",",-1); // get valid options
        int i=0;
        boolean validIs = false;
        itChecksOut:
        for(;!validIs;){ // keep looping until reply valid
            reply = JOptionPane.showInputDialog(this.query);
            reply = reply.toUpperCase();
            for(i=0;i<validArr.length;i++){ // check valid
                if(validArr[i].equals(reply)){ // it is valid
                    validIs=true;
                    break itChecksOut;
                }
            }
            JOptionPane.showMessageDialog(null, "Please try again. Reply is not valid!");
        }
        return reply;
    }
    public void check(){ // check if reply correct
        String reply="";
        reply=this.ask();
        String[] correctArr = this.correct.split(",",-1); // get correct options
        int i=0;
        for(;i<correctArr.length;i++){ // loop correct options
            if(correctArr[i].equals(reply)){ // it is correct
                JOptionPane.showMessageDialog(null, "Correct!");
                this.nCorrect++;
                return;
            }
        }
        JOptionPane.showMessageDialog(null, "Incorrect. The correct reply is "+correctArr[0]+"!");
    }
}
