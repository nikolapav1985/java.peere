import javax.swing.JOptionPane;
/**
* Quiz class
*
* A quiz example. One reply is correct and makes quiz attempt successful.
* All other replies may be valid (but not correct).
* If reply is not valid, the question is asked again (until reply is valid).
*
* compile example ----- javac Quiz.java
*
* run example ------ java Quiz
*
*/
class Quiz{
    public static void main(String[] args){
        int nQuestions = 5; // number of questions of each type
        int nQuestionsTotal = 10; // total number of questions
        int nCorrect = 0; // number of correct attempts
        Question questiona=new MultipleChoiceQuestion("Best method for data input?",
        "A keyboard","B mouse","C gesture","D voice","E stylus","A");
        Question questionb=new MultipleChoiceQuestion("Most popular drink in the morning?",
        "A coffee","B tea","C milk","D orange juice","E apple juice","A");
        Question questionc=new MultipleChoiceQuestion("Best code editor?",
        "A notepad","B notepad++","C emacs","D vim","E visual studio","C");
        Question questiond=new MultipleChoiceQuestion("Easiest dish to make?",
        "A pot roast","B boiled rice","C steamed rice", "D steamed buns", "E fried buns","C");
        Question questione=new MultipleChoiceQuestion("Tastiest meat?",
        "A chicken","B beef","C lamb","D goat","E pork","B");
        Question questionf=new TrueFalseQuestion("Is milk purple?","FALSE,F,NO,N");
        Question questiong=new TrueFalseQuestion("Is current economy in good shape?","FALSE,F,NO,N");
        Question questionh=new TrueFalseQuestion("Is meat pie tasty?","TRUE,T,YES,Y");
        Question questioni=new TrueFalseQuestion("Does pen require ink?","TRUE,T,YES,Y");
        Question questionj=new TrueFalseQuestion("Is book useful?","TRUE,T,YES,Y");
        Question[] questions=new MultipleChoiceQuestion[nQuestions];
        Question[] questionsb=new TrueFalseQuestion[nQuestions];
        int i=0;

        questions[0]=questiona;
        questions[1]=questionb;
        questions[2]=questionc;
        questions[3]=questiond;
        questions[4]=questione;

        questionsb[0]=questionf;
        questionsb[1]=questiong;
        questionsb[2]=questionh;
        questionsb[3]=questioni;
        questionsb[4]=questionj;

        for(;i<nQuestions;i++){ // ask questions, multiple choice
            questions[i].check();
            nCorrect += questions[i].nCorrect;
        }

        for(i=0;i<nQuestions;i++){ // ask questions, true false
            questionsb[i].check();
            nCorrect += questionsb[i].nCorrect;
        }

        JOptionPane.showMessageDialog(null, "The score is "+nCorrect+" out of "+nQuestionsTotal+"!");
    }
}
