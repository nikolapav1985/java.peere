Assignment 5
------------

- Question.java (abstract class to represent a question)
- MultipleChoiceQuestion.java (a class to represent multiple choice question)
- TrueFalseQuestion.java (a class to represent true false question)
- Question.java (a class to make a quiz, check comments for details)

Test environment
----------------

os lubuntu 16.04 javac version 11.0.5
